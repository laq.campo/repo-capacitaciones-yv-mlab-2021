<center>

# BIOGRAFIA 
<img src = https://gitlab.com/laq.campo/repo-capacitaciones-yv-mlab-2021/-/raw/master/imagenes/perfil.jpg width= "200">
</center>

## Datos personales
<p style="color: #ff0">
Mi nombre es Lesly Abigail Campo Quinga, tengo 19 años nací en Quito el 30 de marzo del 2001. Vivi 2 años en Capelo, actualmente vivo en la parroquia de la Merced, con mis padres y hermano.
</p>

## Estudios y Aspiraciones

<p style="">
Estudie en  el “Jardin Gustavo Diez Delgado” ubicada en La Merced, la primaria la curse en la Escuela “Teodoro Wolf” hasta 7mo curso. Para 8vo, 9no, 10mo y bachillerato asistí a la “Unidad Educativa Fiscal Alangasi” que se encuentran en la parroquia de Alangasi en el Valle de los Chillos. Actualmente estudio en el Instituto Tecnologico Superior “Benito Juarez”.

Me gradue en el 2019 con especialidad en Contabilidad.</p>
He asistido a algunos cursos como: 

 - Centro de Capacitacion “DCA sistemas” Quito durante 120 horas con certificado de Operador de computadoras.
 - Centro de Capacitaciones “LAB XXl” Quito
 - SETEC Quito durante 120 horas con certificado de Peluqueria y Estetica.

Me gusta mucho incursionar en la tecnología, aspiro a obtener un puesto en la empresa “Manticore” y asi poder tener una estabilidad economica y ademas de eso modo sumar mis conocimientos y crecer profesionalmente desarrollando mis habilidades y destrezas.


</p>

## Pasatiempos
<p style="color: #ff0">
En mi tiempo libre practico la pintura artística, leo novelas y noticias relacionadas a las tecnologías y su desarrollo,practico basketboll los fines de seman. Tambien me gusta ayudar a a quien lo necesita por lo que formo parte de un grupo de lideres juveniles que tanto del barrio donde vivo como de mi parroquia.
</p>
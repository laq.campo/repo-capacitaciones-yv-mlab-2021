# Menu
- [Informacion de como acceder](#Informacion-de-como-acceder)
- [Crear repositorio](#crear-repositorio)
- [Crear grupos](#crear-grupos)
- [Crear subgrupos](#crear-subgrupos)

- [Crear labels](#crear-labels)
- [Roles que cumplen](#roles-que-cumplen)
- [Agregar miembros](#agregar-miembros)
- [Crear boards y manejos de boards](#Crear-boards-y-manejo-de-boards)

## Informacion de como acceder
1. Nos dirigimos al navegador 
2. https://gitlab.com/
3. Nos registramos
<img src = https://gitlab.com/laq.campo/repo-capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab/iniciogitlab.png width= "1000">


## Crear repositorio
- Nuevo repositorio

<img src = https://gitlab.com/laq.campo/repo-capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab/iniciogitlab.png width= "1000">


## Crear grupos
-Vamos a GRUPOS

<img src = https://gitlab.com/laq.campo/repo-capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab/miembros.png width= "1000">

- Llenamos el formulario 

<img src = https://gitlab.com/laq.campo/repo-capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab/formulario.png width= "1000">

## Crear subgrupos
- Vamos a grupos
  - Subgrupos

<img src = https://gitlab.com/laq.campo/repo-capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab/subgrupo.png width= "1000">

- Llenamos el formulario

<img src = https://gitlab.com/laq.campo/repo-capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab/formulario.png width= "1000">

## Crear labels
<img src = https://gitlab.com/laq.campo/repo-capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab/miembros.png width= "1000">

## Roles que cumplen
- Vamos a repositorio
Roles nos presenta:
1. Mantenedor
2. Critico
3. Desarrollador
4. Contribuyente

## Agregar miembros
Nod dirigimos a la barra izquierda y vamos a miembros.
<img src = https://gitlab.com/laq.campo/repo-capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab/miembros.png width= "1000">

## Crear boards y manejos de boards
- Vamos al Menu
- Cuestiones
- Tableros 

<img src = https://gitlab.com/laq.campo/repo-capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitlab/tableros.png width= "4000">
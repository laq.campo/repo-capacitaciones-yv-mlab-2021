# Menu
- [Que es git](#Que-es-git)
- [Comandos de git en consola](#comandos-de-git-en-consola)
- [Clientes git](#clientes-git)
- [Clonacion de proyectos por consola y por cliente](#clonacion-de-proyectos-por-consola-y.por-cliente)
- [Commits por consola y por cliente kraken o smart](#Commits-por-consola-y-por-cliente-kraken-o-smart)
- [Ramas de kraken](#Ramas-de-kraken)

# Que es git
<p>Es un sistema de control de versiones distribuido de código abierto y gratuito diseñado para manejar todo, desde proyectos pequeños a muy grandes, con velocidad y eficiencia.</p>

# Comandos de git en consola
- git init

Podemos ejecutar ese comando para crear localmente un repositorio con GIT y así utilizar todo el funcionamiento que GIT ofrece.  Basta con estar ubicados dentro de la carpeta donde tenemos nuestro proyecto y ejecutar el comando.  Cuando agreguemos archivos y un commit, se va a crear el branch master por defecto.
 <img src = https://gitlab.com/laq.campo/repo-capacitaciones-yv-mlab-2021/-/raw/master/imagenes/init.png width= "1000">



- git commit -m "mensaje"

Hace commit de los archivos que han sido modificados y GIT los está siguiendo.
 <img src =https://gitlab.com/laq.campo/repo-capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git%20commit.png width= "1000">



- git push

Se usa para cargar contenido del repositorio local a un repositorio remoto
<img src = https://gitlab.com/laq.campo/repo-capacitaciones-yv-mlab-2021/-/raw/master/imagenes/add.png width= "1000">

- Git add.

Este comando añade al índice cualquier fichero nuevo o que haya sido modificado
<img src = https://gitlab.com/laq.campo/repo-capacitaciones-yv-mlab-2021/-/raw/master/imagenes/push.png width= "1000">

## Clientes git
<p>El cliente GIT es el software que instalamos en cada máquina de desarrollo para trabajar con el código, se encargará de hacer peticiones al servidor para obtener los cambios, etc. </p>
<img src = https://gitlab.com/laq.campo/repo-capacitaciones-yv-mlab-2021/-/raw/master/imagenes/gitkraken.png width= "1000">

## Clonacion de proyectos por consola y por cliente
Cuando crea un repositorio en GitHub, existe como un repositorio remoto. Puede clonar su repositorio para crear una copia local en su computadora y sincronizar entre las dos ubicaciones

1. Colocamos git clone (nombre de la carpeta)
<img src = https://gitlab.com/laq.campo/repo-capacitaciones-yv-mlab-2021/-/raw/master/imagenes/push.png width= "1000">


2. Vamos a Kraken 
- File
- clone repo
<img src = https://gitlab.com/laq.campo/repo-capacitaciones-yv-mlab-2021/-/raw/master/imagenes/clonarporkraken1.png width= "1000">

<img src = https://gitlab.com/laq.campo/repo-capacitaciones-yv-mlab-2021/-/raw/master/imagenes/clonkraken2.png width= "1000">


## Commits por consola y por cliente kraken o smart
- Vamos a crear un commit para guardar por consola los cambios realizados
<img src = https://gitlab.com/laq.campo/repo-capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git%20commit.png width= "1000">

- Nos dirigimos a kraken y comentamos en (summary)
<img src = https://gitlab.com/laq.campo/repo-capacitaciones-yv-mlab-2021/-/raw/master/imagenes/summery.png width= "1000">

## Ramas de kraken

- Visualizamos las ramas que nos presenta kraken del proyeto una vez cometadas.
<img src = https://gitlab.com/laq.campo/repo-capacitaciones-yv-mlab-2021/-/raw/master/imagenes/ramas.png width= "1000">



